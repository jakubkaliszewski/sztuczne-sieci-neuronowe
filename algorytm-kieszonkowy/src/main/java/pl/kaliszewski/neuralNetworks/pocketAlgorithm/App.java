package pl.kaliszewski.neuralNetworks.pocketAlgorithm;

import pl.kaliszewski.neuralNetworks.ConsoleArgumentsInterpreter;
import pl.kaliszewski.neuralNetworks.Dataset;

public class App
{
    public static void main( String[] args ){
        Dataset trainDataset, testDataset;

        try{
            ConsoleArgumentsInterpreter.setParameters(args);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return;
        }

        trainDataset = new Dataset(ConsoleArgumentsInterpreter.getTrainFilePath());
        testDataset = new Dataset(ConsoleArgumentsInterpreter.getTestFilePath());

        PocketAlgorithm pocketAlgorithm = new PocketAlgorithm(trainDataset);
        pocketAlgorithm.train(ConsoleArgumentsInterpreter.getIterations(), ConsoleArgumentsInterpreter.getLearnRate());

        try {
            pocketAlgorithm.test(testDataset);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
