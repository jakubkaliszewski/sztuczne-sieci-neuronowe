package pl.kaliszewski.neuralNetworks.pocketAlgorithm;

import pl.kaliszewski.neuralNetworks.Dataset;
import pl.kaliszewski.neuralNetworks.FeaturesSet;

public class PocketAlgorithm {

    private PocketPerceptron perceptron;
    private boolean trained = false;
    private Dataset dataset;
    private final int countOfClasses = 2;
    private int age;

    public PocketAlgorithm(Dataset dataset) {
        this.dataset = dataset;
        perceptron = new PocketPerceptron(this.dataset.getCountOfFeatures());
        perceptron.setBestWeights(perceptron.getWeights().clone());
    }

    public boolean isTrained() {
        return trained;
    }

    //sigm function
    private int activationFunction(double x){
        if (x >= 0)
            return 1;
        else
            return -1;
    }

    private void setBestSet(){
        perceptron.setBestBias(perceptron.getBias());
        perceptron.setBestWeights(perceptron.getWeights().clone());;
        perceptron.setPocketAge(age);
    }

    private void updateWeightsAndBias(FeaturesSet features, double learningRate){
        var actualWeights = perceptron.getWeights();
        var actualBias = perceptron.getBias();
        var actualVector = features.getFeatures();

        actualBias += learningRate * features.getClassName();
        for (int index = 0; index < features.getCountOfFeatures(); index++) {
            actualWeights[index] += learningRate * features.getClassName() * actualVector[index];
        }

        this.age = 0;

        perceptron.setWeights(actualWeights);
        perceptron.setBias(actualBias);
    }

    public void train(int iterations, double learningRate){

        for (int i = 0; i < iterations; i++) {
            double sum = 0.;
            FeaturesSet featuresSet = dataset.getRandomVectorFeatures();
            double[] features = featuresSet.getFeatures().clone();
            double[] weights = perceptron.getWeights().clone();
            for (int j = 0; j < featuresSet.getCountOfFeatures(); j++) {
                sum += (weights[j] * features[j]) + perceptron.getBias();
            }

            if(activationFunction(sum) == featuresSet.getClassName())
                age++;
            else {
                if (age > perceptron.getPocketAge())
                    setBestSet();

                updateWeightsAndBias(featuresSet, learningRate);
            }
        }
        trained = true;
        System.out.println("Koniec treningu perceptronu wg algorytmu kieszonkowego!");
    }

    public void test(Dataset testDataset) throws Exception {
        if(!isTrained())
            throw new Exception("Nie przeprowadzono treningu dla perceptronu!");
        int countOfPassed = 0;

        double[] bestWeights = perceptron.getBestWeights();
        double bestBias = perceptron.getBestBias();

        for (var set : testDataset.getEntireDataset()) {
            double sum = 0.;
            double[] features = set.getFeatures();
            for (int i = 0; i < set.getCountOfFeatures(); i++) {
                sum += (bestWeights[i] * features[i]) + bestBias;
            }

            if(activationFunction(sum) == set.getClassName())
                countOfPassed++;
        }

        System.out.println(String.format("Skuteczność algorytmu kieszonkowego dla %s wynosi: %.2f %%", dataset.getDatasetName(), (double)countOfPassed/(double)testDataset.getCountOfVectors() * 100.));
    }
}
