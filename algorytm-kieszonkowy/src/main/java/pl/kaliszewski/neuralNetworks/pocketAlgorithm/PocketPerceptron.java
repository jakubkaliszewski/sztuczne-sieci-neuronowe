package pl.kaliszewski.neuralNetworks.pocketAlgorithm;

import pl.kaliszewski.neuralNetworks.Perceptron;

public class PocketPerceptron extends Perceptron {

    private double bestBias;
    private double[] bestWeights;
    private int pocketAge;

    public PocketPerceptron(int countOfParameters) {
        super(countOfParameters);
        bias = random();
        bestBias = bias;
    }

    public double getBestBias() {
        return bestBias;
    }

    public void setBestBias(double bestBias) {
        this.bestBias = bestBias;
    }

    public double[] getBestWeights() {
        return bestWeights;
    }

    public void setBestWeights(double[] bestWeights) {
        this.bestWeights = bestWeights;
    }

    public int getPocketAge() {
        return pocketAge;
    }

    public void setPocketAge(int pocketAge) {
        this.pocketAge = pocketAge;
    }
}
