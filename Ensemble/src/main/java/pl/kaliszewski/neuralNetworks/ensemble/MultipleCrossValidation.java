package pl.kaliszewski.neuralNetworks.ensemble;

import weka.classifiers.Classifier;
import weka.core.Instances;
import weka.classifiers.Evaluation;

import java.io.*;
import java.util.Random;

public class MultipleCrossValidation {

    public static void performCrossValidation(int runs, int folds, Instances data, Ensemble ensemble) {
        // perform cross-validation
        try{
            for (int i = 0; i < runs; i++) {
                // randomize data
                int seed = i + 1;
                Random rand = new Random(seed);
                Instances randData = new Instances(data);
                randData.randomize(rand);
                if (randData.classAttribute().isNominal())
                    randData.stratify(folds);

                Evaluation eval = new Evaluation(randData);//wynik dla każdego z komitetów wpisać tutaj, na osobnym jeszcze znów zbiorze
                for (int n = 0; n < folds; n++) {
                    Instances train = randData.trainCV(folds, n, rand);//uczenie każdej jednostki z komitetu
                    Instances test = randData.testCV(folds, n);
                    ensemble.buildClassifier(train);
                    // the above code is used by the StratifiedRemoveFolds filter, the
                    // code below by the Explorer/Experimenter:
                    // Instances train = randData.trainCV(folds, n, rand);

                    // build and evaluate classifier
                    Classifier clsCopy = ensemble.clone();//(Classifier) new ObjectInputStream(new ByteArrayInputStream(cloneObject(classifier))).readObject();
                    clsCopy.buildClassifier(train);
                    eval.evaluateModel(clsCopy, test);//przetrenowany model, zbiór testowy
                }

                //czy w kroswalidacji trenuję  10 razy model, za każdtm razeim na innym zbiorze uczącym i testowym.
                //Czyli sprawdzanie na testowym...wykorzystuję moduł

                // output evaluation
                System.out.println();
                System.out.println("=== Setup run " + (i + 1) + " ===");
                System.out.println("Classifier: " + ensemble.getClass().getName());
                System.out.println("Dataset: " + data.relationName());
                System.out.println("Folds: " + folds);
                System.out.println("Seed: " + seed);
                System.out.println();
                System.out.println(eval.toSummaryString("=== " + folds + "-fold Cross-validation run " + (i + 1) + "===", false));
            }
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
