package pl.kaliszewski.neuralNetworks.ensemble;

import java.util.function.Function;

public enum CombinationRuleEnum {
    AVERAGE_RULE(1, "AVG", "Average of Probabilities", CombinationRuleEnum::ExampleMethod),
    PRODUCT_RULE(2, "PROD", "Product of Probabilities", CombinationRuleEnum::ExampleMethod),
    MAJORITY_VOTING_RULE(3, "MAJ", "Majority Voting", CombinationRuleEnum::ExampleMethod),
    MIN_RULE(4, "MIN", "Minimum Probability", CombinationRuleEnum::ExampleMethod),
    MAX_RULE(5, "MAX", "Maximum Probability", CombinationRuleEnum::ExampleMethod),
    MEDIAN_RULE(6, "MED", "Median", CombinationRuleEnum::ExampleMethod);

    CombinationRuleEnum(int value, String shortName, String fullName, Function<double[], Double> method) {
        this.value = value;
        this.shortName = shortName;
        this.fullName = fullName;
        this.method = method;
    }

    private int value;
    private String shortName;
    private String fullName;
    private Function<double[], Double> method;

    //na wyjściu numer klasy zwycięzcy...
    //tutaj znajduje się f. rozstrzygająca dla komitetu. Na wejściu tablica prawdopodobieństw przynależności, ale czy czasem
    // nie powinna być to taka lista list, odpowiadająca każdemu z klasyfikatorów w komitecie??
    public static double ExampleMethod(double[] x) {
        return -1;
    }

    public String getShortName() {
        return shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public Function<double[], Double> getMethod() {
        return method;
    }
};

