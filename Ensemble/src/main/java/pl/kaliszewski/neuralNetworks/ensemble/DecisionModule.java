package pl.kaliszewski.neuralNetworks.ensemble;

import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Utils;

import java.util.Arrays;
import java.util.List;

class DecisionModule {
    private CombinationRuleEnum combinationRule;

    public DecisionModule(CombinationRuleEnum combinationRule) {
        this.combinationRule = combinationRule;
    }

    //Moduł decyzyjny mający posiadać soft voting
    public double classifyInstance(Instance instance, List<Classifier> classifiers) throws Exception {
        double result;
        double[] dist;

        dist = distributionForInstance(instance, classifiers);

        result = combinationRule.getMethod().apply(dist);
        return result;
    }

    //dystrybucja per klasę, prawdopodobieństwa są zwracane po normalizacji
    private double[] distributionForInstance(Instance instance, List<Classifier> classifiers)
            throws Exception {

        double[] probs = new double[instance.numClasses()];
        Arrays.fill(probs, 1.0);

        int numPredictions = 0;
        for (int i = 0; i < classifiers.size(); i++) {
            double[] dist = classifiers.get(i).distributionForInstance(instance);
            if (Utils.sum(dist) > 0) {
                for (int j = 0; j < dist.length; j++) {
                    probs[j] *= dist[j];
                }
                numPredictions++;
            }
        }

        // No predictions?
        if (numPredictions == 0) {
            return new double[instance.numClasses()];
        }

        // Should normalize to get "probabilities"
        if (Utils.sum(probs) > 0) {
            Utils.normalize(probs);
        }

        return probs;
    }


    /**
     * Classifies the given test instance.
     *
     * @param instance the instance to be classified
     * @return the predicted most likely class for the instance or
     *         Utils.missingValue() if no prediction is made
     * @throws Exception if an error occurred during the prediction
     */
    /*
    @Override
    public double classifyInstance(Instance instance) throws Exception {
        double result;
        double[] dist;
        int index;

        switch (m_CombinationRule) {
            case AVERAGE_RULE:
            case PRODUCT_RULE:
            case MAJORITY_VOTING_RULE:
            case MIN_RULE:
            case MAX_RULE:
                dist = distributionForInstance(instance);
                if (instance.classAttribute().isNominal()) {
                    index = Utils.maxIndex(dist);
                    if (dist[index] == 0) {
                        result = Utils.missingValue();
                    } else {
                        result = index;
                    }
                } else if (instance.classAttribute().isNumeric()) {
                    result = dist[0];
                } else {
                    result = Utils.missingValue();
                }
                break;
            case MEDIAN_RULE:
                result = classifyInstanceMedian(instance);
                break;
            default:
                throw new IllegalStateException("Unknown combination rule '"
                        + m_CombinationRule + "'!");
        }

        return result;
    }

    /*

    /*
    Odpowiednik modułu decyzyjnego
    Classifies a given instance using the selected combination rule.
   *
           * @param instance the instance to be classified
   * @return the distribution
   * @throws Exception if instance could not be classified successfully
   *
    @Override
    public double[] distributionForInstance(Instance instance) throws Exception {
        double[] result = new double[instance.numClasses()];

        switch (m_CombinationRule) {
            case AVERAGE_RULE:
                result = distributionForInstanceAverage(instance);
                break;
            case PRODUCT_RULE:
                result = distributionForInstanceProduct(instance);
                break;
            case MAJORITY_VOTING_RULE:
                result = distributionForInstanceMajorityVoting(instance);
                break;
            case MIN_RULE:
                result = distributionForInstanceMin(instance);
                break;
            case MAX_RULE:
                result = distributionForInstanceMax(instance);
                break;
            case MEDIAN_RULE:
                result[0] = classifyInstance(instance);
                break;
            default:
                throw new IllegalStateException("Unknown combination rule '"
                        + m_CombinationRule + "'!");
        }

        if (!instance.classAttribute().isNumeric() && (Utils.sum(result) > 0)) {
            Utils.normalize(result);
        }

        return result;
    }
    */
}
