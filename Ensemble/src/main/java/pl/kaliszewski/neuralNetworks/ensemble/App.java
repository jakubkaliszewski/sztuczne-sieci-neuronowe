package pl.kaliszewski.neuralNetworks.ensemble;

import weka.classifiers.Classifier;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ConverterUtils;

//TODO przetłumaczyć
/**
 *  * Command-line parameters:
 *  * <ul>
 *  *    <li>-t filename - the dataset to use</li>
 *  *    <li>-x int - the number of folds to use</li>
 *  *    <li>-r int - the number of runs to perform</li>
 *  *    <li>-c int - the class index, "first" and "last" are accepted as well;
 *  *    "last" is used by default</li>
 *  *    <li>-W classifier - classname and options, enclosed by double quotes;
 *  *    the classifier to cross-validate</li>
 *  * </ul>
 *  *
 *  * Example command-line:
 *  * <pre>
 *  * java appName -t labor.arff -c last -x 10 -r 10 -W "weka.classifiers.trees.J48 -C 0.25"
 *  * </pre>
 */
public class App {
    public static void main(String[] args) {
        Instances data;
        String clsIndex;
        String[] tmpOptions;
        String classname;
        int runs, folds;

        try{
            // loads data and set class index
            data = ConverterUtils.DataSource.read(Utils.getOption("t", args));
            clsIndex = Utils.getOption("c", args);
            if (clsIndex.length() == 0)
                clsIndex = "last";
            if (clsIndex.equals("first"))
                data.setClassIndex(0);
            else if (clsIndex.equals("last"))
                data.setClassIndex(data.numAttributes() - 1);
            else
                data.setClassIndex(Integer.parseInt(clsIndex) - 1);

            //klasyfikator
            tmpOptions     = Utils.splitOptions(Utils.getOption("W", args));
            classname      = tmpOptions[0];
            tmpOptions[0]  = "";
            Classifier classifier = (Classifier) Utils.forName(Classifier.class, classname, tmpOptions);

            //inne opcje
            runs  = Integer.parseInt(Utils.getOption("r", args));
            folds = Integer.parseInt(Utils.getOption("x", args));
        }catch (Exception e){
            System.out.println("Napotkano błąd przy wczytywaniu ustawień!\nOpuszczanie programu...");
            return;
        }
    }
}
