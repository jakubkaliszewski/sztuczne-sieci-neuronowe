package pl.kaliszewski.neuralNetworks.ensemble;

import weka.classifiers.Classifier;
import weka.classifiers.RandomizableMultipleClassifiersCombiner;
import weka.classifiers.meta.Vote;
import weka.classifiers.misc.InputMappedClassifier;
import weka.core.*;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.LinkedList;
import java.util.List;

public class Ensemble extends RandomizableMultipleClassifiersCombiner implements Cloneable {
    private final List<Classifier> m_preBuiltClassifiers;
    /** Structure of the training data */
    protected Instances m_structure;
    DecisionModule decisionModule;
    boolean trained;
    /**
     * Combination Rule variable
     */
    protected CombinationRuleEnum defaultCombinationRule = CombinationRuleEnum.AVERAGE_RULE;// na usuniecie bd
    Vote vote;//na wzór, komitet z Vega

    public Ensemble() {
        decisionModule = new DecisionModule(CombinationRuleEnum.AVERAGE_RULE);
        this.m_preBuiltClassifiers = new LinkedList<>();
        trained = false;
    }

    public void addClassifier(Classifier classifier) {
        this.m_preBuiltClassifiers.add(classifier);
    }

    /**
     * Buildclassifier selects a classifier from the set of classifiers by
     * minimising error on the training data.
     *
     * @param data the training data to be used for generating the boosted
     *          classifier.
     * @throws Exception if the classifier could not be built successfully
     */

    @Override
    public void buildClassifier(Instances data) throws Exception {//gdzie podłączyć moduł rozstzygający

        // remove instances with missing class
        Instances newData = new Instances(data);
        newData.deleteWithMissingClass();
        m_structure = new Instances(newData, 0);

        // can classifier handle the data?
        getCapabilities().testWithFail(data);

        for (int i = 0; i < m_Classifiers.length; i++) {
            getClassifier(i).buildClassifier(newData);
        }
    }

    /**
     * Returns default capabilities of the classifier.
     *
     * @return the capabilities of this classifier
     */
    @Override
    public Capabilities getCapabilities() {
        Capabilities result = super.getCapabilities();

        if (m_preBuiltClassifiers.size() > 0) {
            if (m_Classifiers.length == 0) {
                result =
                        (Capabilities) m_preBuiltClassifiers.get(0).getCapabilities().clone();
            }
            for (int i = 1; i < m_preBuiltClassifiers.size(); i++) {
                result.and(m_preBuiltClassifiers.get(i).getCapabilities());
            }

            for (Capabilities.Capability cap : Capabilities.Capability.values()) {
                result.enableDependency(cap);
            }
        }

        // class
        result.disableAllClasses();
        result.disableAllClassDependencies();
        result.enable(Capabilities.Capability.NUMERIC_CLASS);
        result.enableDependency(Capabilities.Capability.NUMERIC_CLASS);

        return result;
    }

    @Override
    public double classifyInstance(Instance instance) throws Exception {
        return super.classifyInstance(instance);
    }

    /**
     * Classifies the given test instance.
     *
     * @param instance the instance to be classified
     * @return the predicted most likely class for the instance or
     *         Utils.missingValue() if no prediction is made
     * @throws Exception if an error occurred during the prediction
     */
    /*
    @Override
    public double classifyInstance(Instance instance) throws Exception {
        double result;
        double[] dist;
        int index;

        switch (m_CombinationRule) {
            case AVERAGE_RULE:
            case PRODUCT_RULE:
            case MAJORITY_VOTING_RULE:
            case MIN_RULE:
            case MAX_RULE:
                dist = distributionForInstance(instance);
                if (instance.classAttribute().isNominal()) {
                    index = Utils.maxIndex(dist);
                    if (dist[index] == 0) {
                        result = Utils.missingValue();
                    } else {
                        result = index;
                    }
                } else if (instance.classAttribute().isNumeric()) {
                    result = dist[0];
                } else {
                    result = Utils.missingValue();
                }
                break;
            case MEDIAN_RULE:
                result = classifyInstanceMedian(instance);
                break;
            default:
                throw new IllegalStateException("Unknown combination rule '"
                        + m_CombinationRule + "'!");
        }

        return result;
    }

    /*

    /*
    Odpowiednik modułu decyzyjnego
    Classifies a given instance using the selected combination rule.
   *
           * @param instance the instance to be classified
   * @return the distribution
   * @throws Exception if instance could not be classified successfully
   *
    @Override
    public double[] distributionForInstance(Instance instance) throws Exception {
        double[] result = new double[instance.numClasses()];

        switch (m_CombinationRule) {
            case AVERAGE_RULE:
                result = distributionForInstanceAverage(instance);
                break;
            case PRODUCT_RULE:
                result = distributionForInstanceProduct(instance);
                break;
            case MAJORITY_VOTING_RULE:
                result = distributionForInstanceMajorityVoting(instance);
                break;
            case MIN_RULE:
                result = distributionForInstanceMin(instance);
                break;
            case MAX_RULE:
                result = distributionForInstanceMax(instance);
                break;
            case MEDIAN_RULE:
                result[0] = classifyInstance(instance);
                break;
            default:
                throw new IllegalStateException("Unknown combination rule '"
                        + m_CombinationRule + "'!");
        }

        if (!instance.classAttribute().isNumeric() && (Utils.sum(result) > 0)) {
            Utils.normalize(result);
        }

        return result;
    }
    */

    @Override
    protected Ensemble clone() {
        try {
            return (Ensemble) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(this.getClass().getName() + " nie implementuje Cloneable!");
            return null;
        }
    }
}
