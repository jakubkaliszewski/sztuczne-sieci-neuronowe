package pl.kaliszewski.neuralNetworks.linearMachine;

import pl.kaliszewski.neuralNetworks.ConsoleArgumentsInterpreter;
import pl.kaliszewski.neuralNetworks.Dataset;

public class App {
    public static void main(String[] args) {
        Dataset trainDataset, testDataset;

        try{
            ConsoleArgumentsInterpreter.setParameters(args);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return;
        }

        trainDataset = new Dataset(ConsoleArgumentsInterpreter.getTrainFilePath());
        testDataset = new Dataset(ConsoleArgumentsInterpreter.getTestFilePath());

        LinearMachine linearMachine = new LinearMachine(trainDataset);
        linearMachine.train(ConsoleArgumentsInterpreter.getIterations(), ConsoleArgumentsInterpreter.getLearnRate());

        linearMachine.test(testDataset);
    }
}
