package pl.kaliszewski.neuralNetworks.linearMachine;

import pl.kaliszewski.neuralNetworks.Dataset;
import pl.kaliszewski.neuralNetworks.FeaturesSet;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LinearMachine {
    private List<LinearMachinePerceptron> perceptrons;
    private boolean trained = false;
    private Dataset dataset;
    private int countOfClasses;

    public LinearMachine(Dataset dataset) {
        this.dataset = dataset;
        this.countOfClasses = dataset.getCountOfClasses();
        perceptrons = new ArrayList(countOfClasses);
        for (int i = 0; i < dataset.getCountOfClasses(); i++) {
            perceptrons.add(new LinearMachinePerceptron(dataset.getCountOfFeatures(), i+1));
        }
    }

    public void train(int iterations){
        train(iterations, 1.);
    }

    public void train(int iterations, double learningRate){

        for (int i = 0; i < iterations; i++) {
            var sample = dataset.getRandomVectorFeatures();
            int predictionClass = prediction(sample);

            if(predictionClass != sample.getClassName()){
                try {
                    updateWeightsPositive(getPerceptronFromClass(sample.getClassName()), sample.getFeatures(), learningRate);
                    updateWeightsNegative(getPerceptronFromClass(predictionClass), sample.getFeatures() ,learningRate);
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }
        }

        trained = true;
        System.out.println("Koniec treningu maszyny liniowej!");
    }


    public int prediction(FeaturesSet sample){
        double maximumSum = 0.;
        int predictionClass = 0;

        for (var perceptron : perceptrons) {
            var sum = getWeightedSum(perceptron, sample);
            if (sum >= maximumSum){
                maximumSum = sum;
                predictionClass = perceptron.getClassName();
            }
        }

        return predictionClass;
    }

    private void updateWeightsPositive(LinearMachinePerceptron perceptron, double[] features, double learningRate){
        var actualWeights = perceptron.getWeights();
        var actualBias = perceptron.getBias();

        for (int i = 0; i < features.length; i++) {
            actualWeights[i] += learningRate * features[i];
        }

        perceptron.setBias(actualBias+learningRate);
        perceptron.setWeights(actualWeights);
    }

    private void updateWeightsNegative(LinearMachinePerceptron perceptron, double[] features, double learningRate){
        var actualWeights = perceptron.getWeights();
        var actualBias = perceptron.getBias();

        for (int i = 0; i < features.length; i++) {
            actualWeights[i] -= learningRate * features[i];
        }

        perceptron.setBias(actualBias-learningRate);
        perceptron.setWeights(actualWeights);
    }

    private double getWeightedSum(LinearMachinePerceptron perceptron, FeaturesSet featuresSet){
        double sum = 0.;
        var weights = perceptron.getWeights();
        var features = featuresSet.getFeatures();

        for (int i = 0; i < weights.length; i++) {
            sum += weights[i] * features[i];
        }

        return sum;
    }

    public void test(Dataset testDataset){
        List<Result> results = new ArrayList<>(countOfClasses);
        for (int i = 1; i <=countOfClasses; i++) {
             results.add(new Result(i));
        }

        for(var sample : testDataset.getEntireDataset()){
            var result = results.stream().filter(element -> element.getClassName() == sample.getClassName()).findAny().get();
            int predictionValue = prediction(sample);

            if(sample.getClassName() == predictionValue)
                result.setPositivies();
            else result.setNegativies();
        }

        for (var result: results) {
            System.out.println(result);
        }
    }

    public boolean isTrained() {
        return trained;
    }

    private LinearMachinePerceptron getPerceptronFromClass(int perceptronClass) throws Exception {
        var results = perceptrons.stream().filter(perceptron -> perceptron.getClassName() == perceptronClass).collect(Collectors.toList());
        if (results.size() == 1)
            return results.get(0);
        else {
            throw new Exception(String.format("Nie znaleziono perceptronu odpowiadającego podanej klasie %s", perceptronClass));
        }
    }
}
