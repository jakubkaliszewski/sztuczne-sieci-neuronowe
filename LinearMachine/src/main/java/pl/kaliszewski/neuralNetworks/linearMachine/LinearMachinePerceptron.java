package pl.kaliszewski.neuralNetworks.linearMachine;

public class LinearMachinePerceptron extends pl.kaliszewski.neuralNetworks.Perceptron{

    private int className;

    public LinearMachinePerceptron(int countOfParameters, int myClass) {
        super(countOfParameters);
        className = myClass;
    }

    public int getClassName() {
        return className;
    }
}
