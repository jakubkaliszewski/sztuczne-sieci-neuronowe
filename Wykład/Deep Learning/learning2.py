# encoding: utf-8
# author: Jakub Kaliszewski (kontakt@jakubkaliszewski.pl)
# works on python 3.7.x

# Part 1 - Building the CNN

# Importing the Keras libraries and packages
from tensorflow.python.platform import gfile
import tensorflow as tf
from keras import backend as K
from keras.models import Sequential
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.layers import Dropout

import numpy as np
import pandas as pd
from keras.preprocessing.image import ImageDataGenerator, load_img
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import random
import os


# parameter ==========================
wkdir = '/home/kalisz/Repozytoria/Magisterskie/sztuczne-sieci-neuronowe/Wykład/Deep Learning'
modelFileName = 'cats_dogs_model.h5'

# Initialising the CNN
model = Sequential()

# First convolutional layer
model.add(Convolution2D(
    32, 3, 3, input_shape=(64, 64, 3), activation='relu', bias_initializer='zeros', use_bias=True))
model.add(MaxPooling2D(pool_size=(2, 2)))

# Second convolutional layer
model.add(Convolution2D(64, 3, 3, activation='relu', use_bias=True))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

# Third convolutional layer
model.add(Convolution2D(128, 3, 3, activation='relu', use_bias=True))
model.add(MaxPooling2D(pool_size=(4, 4)))
model.add(Dropout(0.25))


# Step 3 - Flattening
model.add(Flatten())

# Step 4 - Full connection
model.add(Dense(output_dim=512, activation='relu', use_bias=True))
model.add(Dropout(0.5))
model.add(Dense(output_dim=2, activation='softmax'))

# Compiling the CNN
model.compile(
    optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

model.summary()

# Part 2 - Fitting the CNN to the images

filenames = os.listdir(wkdir+'/Dataset/training_set')
categories = []
for filename in filenames:
    category = filename.split('.')[0]
    if category == 'dog':
        categories.append(1)
    else:
        categories.append(0)

df = pd.DataFrame({
    'filename': filenames,
    'category': categories
})


df.head()

df["category"] = df["category"].replace({0: 'cat', 1: 'dog'})

train_df, validate_df = train_test_split(df, test_size=0.20, random_state=42)
train_df = train_df.reset_index(drop=True)
validate_df = validate_df.reset_index(drop=True)


total_train = train_df.shape[0]
total_validate = validate_df.shape[0]
batch_size = 100

train_datagen = ImageDataGenerator(rescale=1./255,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   horizontal_flip=True,
                                   rotation_range=15)

test_datagen = ImageDataGenerator(rescale=1./255)


train_generator = train_datagen.flow_from_dataframe(
                                                train_df,
                                                'Dataset/training_set',
                                                x_col='filename',
                                                y_col='category',
                                                target_size=(64, 64),
                                                class_mode='categorical',
                                                batch_size=batch_size,
                                                shuffle=True
                                                )


validation_datagen = ImageDataGenerator(rescale=1./255)
validation_generator = validation_datagen.flow_from_dataframe(
                                                validate_df,
                                                'Dataset/training_set',
                                                x_col='filename',
                                                y_col='category',
                                                target_size=(64,64),
                                                class_mode='categorical',
                                                batch_size=batch_size,
                                                shuffle=True
)

history = model.fit_generator(
    train_generator,
    epochs=50,
    validation_data=validation_generator,
    validation_steps=total_validate/batch_size,
    steps_per_epoch=total_train/batch_size,
)

model.save_weights(modelFileName)

#Virtualize Training
fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(12, 12))
ax1.plot(history.history['loss'], color='b', label="Training loss")
ax1.plot(history.history['val_loss'], color='r', label="validation loss")
ax1.set_xticks(np.arange(1, 50, 1))
ax1.set_yticks(np.arange(0, 1, 0.1))

ax2.plot(history.history['acc'], color='b', label="Training accuracy")
ax2.plot(history.history['val_acc'], color='r', label="Validation accuracy")
ax2.set_xticks(np.arange(1, 50, 1))

legend = plt.legend(loc='best', shadow=True)
plt.tight_layout()
plt.show()


#Testing
test_filenames = os.listdir(wkdir+"/Dataset/test_set")
test_df = pd.DataFrame({
    'filename': test_filenames
})

nb_samples = test_df.shape[0]
test_gen = ImageDataGenerator(rescale=1./255)
test_generator = test_gen.flow_from_dataframe(
    test_df,
    "Dataset/test_set",
    x_col='filename',
    y_col=None,
    class_mode=None,
    target_size=(64,64),
    batch_size=batch_size,
    shuffle=False
)

#Predict
predict = model.predict_generator(
    test_generator, steps=np.ceil(nb_samples/batch_size))
test_df['category'] = np.argmax(predict, axis=-1)
label_map = dict((v, k) for k, v in train_generator.class_indices.items())
test_df['category'] = test_df['category'].replace(label_map)

test_df['category'] = test_df['category'].replace({'dog': 1, 'cat': 0})

sample_test = test_df.head(18)
sample_test.head()
plt.figure(figsize=(12, 24))
for index, row in sample_test.iterrows():
    filename = row['filename']
    category = row['category']
    img = load_img("Dataset/test_set/"+filename, target_size=(64,64))
    plt.subplot(6, 3, index+1)
    plt.imshow(img)
    plt.xlabel(filename + '(' + "{}".format(category) + ')')
plt.tight_layout()
plt.show()
