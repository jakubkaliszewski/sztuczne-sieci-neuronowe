package pl.kaliszewski.neuralNetworks.linearMachine;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

//zrobić, że po wyuczeniu modelu zawsze jest on zapisywany z nazwą LinearMachine_data_godzina.json
//dodać do interpretatora poleceń - import nazwapliku.json


public class Serialization {
    private static Gson _gson = new Gson();

    public static void ExportLinearMachineModelToJSON(List<LinearMachinePerceptron> learnedPerceptrons){

        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");
        String filename = "LinearMachineModel_" + dateTime.format(dateFormat) + ".json";

        try (Writer writer = new FileWriter(filename)) {
            _gson.toJson(learnedPerceptrons, writer);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static List<LinearMachinePerceptron> ImportLinearMachineModelFromJSON(String jsonFile) throws Exception{
        if(jsonFile == null)
            throw new Exception("Nie podano pliku z modelem do zaimportowania!");

        Type perceptronsListType = new TypeToken<ArrayList<LinearMachinePerceptron>>(){}.getType();
        JsonReader reader = new JsonReader(new FileReader(jsonFile));
        List<LinearMachinePerceptron> importedData = _gson.fromJson(reader, perceptronsListType);

        return importedData;
    }
}
