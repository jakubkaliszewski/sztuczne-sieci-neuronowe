package pl.kaliszewski.neuralNetworks.linearMachine;

public class Result {
    private int className;
    private double positivies;
    private double total;

    public Result(int className) {
        this.className = className;
    }

    public void setPositivies() {
        this.positivies++;
        this.total++;
    }

    public void setNegativies() {
        this.total++;
    }

    @Override
    public String toString() {
        return String.format("Rezultat dla klasy %s wynosi: %.2f %%", className, positivies/total * 100);
    }

    public int getClassName() {
        return className;
    }
}
