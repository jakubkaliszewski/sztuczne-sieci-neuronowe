package pl.kaliszewski.neuralNetworks.linearMachine;

import pl.kaliszewski.neuralNetworks.ConfusionMatrix;
import pl.kaliszewski.neuralNetworks.Dataset;
import pl.kaliszewski.neuralNetworks.FeaturesSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class LinearMachine {
    private List<LinearMachinePerceptron> perceptrons;
    private boolean trained = false;
    private Dataset dataset;
    private int countOfClasses;

    public LinearMachine(Dataset dataset) {
        this.dataset = dataset;
        this.countOfClasses = dataset.getCountOfClasses();
        perceptrons = new ArrayList(countOfClasses);
        for (var clas:dataset.getClasses()) {
            perceptrons.add(new LinearMachinePerceptron(dataset.getCountOfFeatures(), clas));
        }
    }

    public LinearMachine(List<LinearMachinePerceptron> importedPerceptrons){
        perceptrons = importedPerceptrons;
        trained = true;
        countOfClasses = perceptrons.size();
    }

    public void train(int iterations){
        train(iterations, 1.);
    }

    public void train(int iterations, double learningRate){

        for (int i = 0; i < iterations; i++) {
            var sample = dataset.getRandomVectorFeatures();
            int predictionClass = prediction(sample);

            if(predictionClass != sample.getClassName()){
                try {
                    updateWeightsPositive(getPerceptronFromClass(sample.getClassName()), sample.getFeatures(), learningRate);
                    updateWeightsNegative(getPerceptronFromClass(predictionClass), sample.getFeatures() ,learningRate);
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }
        }

        trained = true;
        System.out.println("Koniec treningu maszyny liniowej!");
    }


    public int prediction(FeaturesSet sample){
        double maximumSum = 0.;
        int predictionClass = 0;

        for (var perceptron : perceptrons) {
            var sum = getWeightedSum(perceptron, sample);
            if (sum >= maximumSum){
                maximumSum = sum;
                predictionClass = perceptron.getClassName();
            }
        }

        return predictionClass;
    }

    private void updateWeightsPositive(LinearMachinePerceptron perceptron, double[] features, double learningRate){
        var actualWeights = perceptron.getWeights();
        var actualBias = perceptron.getBias();

        for (int i = 0; i < features.length; i++) {
            actualWeights[i] += learningRate * features[i];
        }

        perceptron.setBias(actualBias+learningRate);
        perceptron.setWeights(actualWeights);
    }

    private void updateWeightsNegative(LinearMachinePerceptron perceptron, double[] features, double learningRate){
        var actualWeights = perceptron.getWeights();
        var actualBias = perceptron.getBias();

        for (int i = 0; i < features.length; i++) {
            actualWeights[i] -= learningRate * features[i];
        }

        perceptron.setBias(actualBias-learningRate);
        perceptron.setWeights(actualWeights);
    }

    private double getWeightedSum(LinearMachinePerceptron perceptron, FeaturesSet featuresSet){
        double sum = 0.;
        var weights = perceptron.getWeights();
        var features = featuresSet.getFeatures();

        for (int i = 0; i < weights.length; i++) {
            sum += weights[i] * features[i];
        }

        return sum;
    }

    public void test(Dataset testDataset){
        List<Result> results = new ArrayList<>(testDataset.getCountOfVectors());
        ConfusionMatrix confusionMatrix = new ConfusionMatrix(this.dataset.getClasses());

        for (var clas:testDataset.getClasses()) {
            results.add(new Result(clas));
        }

        for(var sample : testDataset.getEntireDataset()){
            Optional<Result> resultOptional = results.stream().filter(element -> element.getClassName() == sample.getClassName()).findAny();
            if(resultOptional.isPresent()){
                var result = resultOptional.get();
                int predictionValue = prediction(sample);

                if(sample.getClassName() == predictionValue)
                    result.setPositivies();
                else result.setNegativies();
                confusionMatrix.addResult(predictionValue, sample.getClassName());
            }
        }

        for (var result: results) {
            System.out.println(result);
        }
        System.out.println("Confusion matrix: ");
        System.out.println(confusionMatrix);
    }

    public boolean isTrained() {
        return trained;
    }

    private LinearMachinePerceptron getPerceptronFromClass(int perceptronClass) throws Exception {
        var results = perceptrons.stream().filter(perceptron -> perceptron.getClassName() == perceptronClass).collect(Collectors.toList());
        if (results.size() == 1)
            return results.get(0);
        else {
            throw new Exception(String.format("Nie znaleziono perceptronu odpowiadającego podanej klasie %s", perceptronClass));
        }
    }

    void exportToFile() {
        Serialization.ExportLinearMachineModelToJSON(perceptrons);
    }
}
