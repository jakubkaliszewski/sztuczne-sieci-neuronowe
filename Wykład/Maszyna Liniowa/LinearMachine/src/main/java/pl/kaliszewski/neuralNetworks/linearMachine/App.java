package pl.kaliszewski.neuralNetworks.linearMachine;

import pl.kaliszewski.neuralNetworks.ConsoleArgumentsInterpreter;
import pl.kaliszewski.neuralNetworks.Dataset;

import java.util.List;

public class App {
    public static void main(String[] args) {
        Dataset trainDataset = null, testDataset = null, predictionSet = null;
        List<LinearMachinePerceptron> importedPerceptrons = null;
        LinearMachine linearMachine = null;

        try{
            ConsoleArgumentsInterpreter.setParameters(args);

            trainDataset = new Dataset(ConsoleArgumentsInterpreter.getTrainFilePath());
            if (ConsoleArgumentsInterpreter.getTestFilePath() != null)
                testDataset = new Dataset(ConsoleArgumentsInterpreter.getTestFilePath());
            if (ConsoleArgumentsInterpreter.getPredictionFilePath() != null)
            predictionSet = new Dataset(ConsoleArgumentsInterpreter.getPredictionFilePath());

            importedPerceptrons = Serialization.ImportLinearMachineModelFromJSON(ConsoleArgumentsInterpreter.getImportFilePath());
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }

        //Nastąpi wyuczenie modelu
        if (importedPerceptrons == null){
            if(testDataset == null){
                testDataset = Dataset.CreateTestDatasetFromTrain(trainDataset, 15);
            }

            linearMachine = new LinearMachine(trainDataset);
            linearMachine.train(ConsoleArgumentsInterpreter.getIterations(), ConsoleArgumentsInterpreter.getLearnRate());

            linearMachine.test(testDataset);
        }
        //Nastąpi wczytanie modelu
        else{
            if(predictionSet == null){
                System.out.println("Plik z zestawami do predykcji nie został odnaleziony!");
            }
            else {
                linearMachine = new LinearMachine(importedPerceptrons);
                for (var features:predictionSet.getEntireDataset()) {
                    System.out.println(features + " .\nPredykcja: " + linearMachine.prediction(features));
                }
            }
        }

        if(linearMachine != null)
            linearMachine.exportToFile();
    }
}
