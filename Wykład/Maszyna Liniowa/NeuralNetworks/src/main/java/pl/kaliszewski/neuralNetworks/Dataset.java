package pl.kaliszewski.neuralNetworks;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Zawiera zbiór danych uprzednio wczytany z pliku
 */
public class Dataset {

    private int countOfFeatures;
    private int countOfVectors;
    private List<Integer> classes;
    private int countOfclasses;
    private String datasetName;

    private LinkedList<FeaturesSet> ListOfFeatures;

    public Dataset(String filename) {
        try {
            loadDatasetFromFile(filename);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public Dataset() {
        ListOfFeatures = new LinkedList<>();
    }

    /**
     * Wczytuje spreparowany plik z danymi. Na jego podstawie ustawiane są takie parametry jak:
     * <ul>
     *     <li>liczba cech,</li>
     *     <li>liczba wektorów,</li>
     *     <li>liczba klas do klasyfikacji</li>
     * </ul>
     * @param filename Plik z danymi do wczytania
     */
    private void loadDatasetFromFile(String filename) throws Exception {
        if(filename == null)
            throw new Exception("Nie podano plików źródłowych!");

        classes = new LinkedList<>();
        datasetName = filename;

        try (Stream<String> lines = Files.lines(Paths.get(filename), Charset.defaultCharset())) {
            ListOfFeatures = new LinkedList<>();

            lines.forEach(line -> {
                if(!line.isEmpty()){
                    String[] splitedLine = line.split(";");
                    countOfFeatures = splitedLine.length - 1;
                    double[] vector = new double[countOfFeatures];

                    for (int i = 0; i < countOfFeatures; i++) {
                        if(!splitedLine[i].isEmpty())
                            vector[i] = Double.parseDouble(splitedLine[i]);
                    }

                    int sampleClass = Integer.parseInt(splitedLine[splitedLine.length - 1]);
                    if(!classes.contains(sampleClass))
                        classes.add(sampleClass);

                    FeaturesSet set = new FeaturesSet(vector, sampleClass);
                    ListOfFeatures.add(set);
                    countOfVectors++;
                }
            });

            classes.sort(Integer::compareTo);
            countOfclasses = classes.size();
        }
        catch (IOException exception){
            System.out.printf("Błąd odczytu pliku %s! Wyjątek: %s", filename, exception.toString());
            throw exception;
        }
    }

    public FeaturesSet getRandomVectorFeatures() {
        int countOfElements = ListOfFeatures.size();
        return ListOfFeatures.get(random(countOfElements));
    }

    public List<FeaturesSet> getEntireDataset(){
        return this.ListOfFeatures;
    }

    private int random(int countOfElements){
        double random = Math.random() * (double) countOfElements;
        return (int)random;
    }

    public void normalize(){
        //normalizować kolumnami
        for (int i = 0; i < countOfFeatures; i++) {//zmiana kolumn
            double max = Double.MIN_VALUE;
            double min = Double.MAX_VALUE;

            for (int j = 0; j < countOfVectors; j++) {//zmiana wierszy - szukanie min i max
                var element = ListOfFeatures.get(j).getFeatures()[i];

                if(element >= max)
                    max = element;

                if(element < min)
                    min = element;
            }

            for (int j = 0; j < countOfVectors; j++) {//faktyczna normalizacja elementów w kolumnie
                var featuresSet = ListOfFeatures.get(j);
                var features = featuresSet.getFeatures();

                features[i] = (features[i] - min) / (max - min);
                //featuresSet.setFeatures(features);
                //ListOfFeatures.set(j, );//.get(j).getFeatures()[i];
            }

        }
    }

    public static Dataset CreateTestDatasetFromTrain(Dataset trainDataset, int percent){
        Dataset testDataset = new Dataset();
        int vectorsToCut = Math.toIntExact(Math.round(trainDataset.countOfVectors * (percent / 100.)));

        testDataset.countOfclasses = trainDataset.countOfclasses;
        testDataset.classes = trainDataset.classes;
        testDataset.countOfFeatures = trainDataset.countOfFeatures;
        testDataset.datasetName = trainDataset.datasetName + " TEST";

        for (int i = 0; i < vectorsToCut; i++) {
            FeaturesSet set = trainDataset.getRandomVectorFeatures();
            trainDataset.ListOfFeatures.remove(set);
            trainDataset.countOfVectors--;

            testDataset.ListOfFeatures.add(set);
            testDataset.countOfVectors++;
        }

        return testDataset;
    }

    public List<Integer> getClasses() {
        return classes;
    }
    public int getCountOfFeatures() {
        return countOfFeatures;
    }
    public String getDatasetName() {
        return datasetName;
    }
    public int getCountOfVectors() {
        return countOfVectors;
    }
    public int getCountOfClasses() { return countOfclasses; }
}
