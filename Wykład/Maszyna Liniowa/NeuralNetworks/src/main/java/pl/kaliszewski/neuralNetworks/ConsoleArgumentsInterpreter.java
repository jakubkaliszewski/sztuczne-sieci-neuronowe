package pl.kaliszewski.neuralNetworks;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class ConsoleArgumentsInterpreter {

    private static Double learnRate;
    private static Integer iterations;
    private static String trainFilePath;
    private static String testFilePath;
    private static String importFilePath;
    private static String predictionFilePath;
    private final static List<String> parameters = Arrays.asList("-train", "-test", "-iter", "-rate", "-import", "-prediction");

    public static void setParameters(String[] args) throws Exception {
        for (int i = 0; i < args.length; i+=2) {
            switch (args[i]){
                case "-train":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1]))
                        trainFilePath = Paths.get("").toAbsolutePath() + "/" + args[i+1];
                    else
                        throw new Exception("Podano zły argument dla parametru \"-train\"");
                    break;
                }
                case "-test":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1]))
                        testFilePath = Paths.get("").toAbsolutePath() + "/" + args[i+1];
                    else
                        throw new Exception("Podano zły argument dla parametru \"-test\"");
                    break;
                }
                case "-iter":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1])) {
                        try {
                            iterations = Integer.parseInt(args[i+1]);
                        }
                        catch (Exception e){
                            throw new Exception("Podano zły argument dla parametru \"-iter\"");
                        }
                    }
                    else
                        throw new Exception("Podano zły argument dla parametru \"-iter\"");
                    break;
                }
                case "-rate":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1])){
                        try {
                            learnRate = Double.parseDouble(args[i+1]);
                        }
                        catch (Exception e){
                            throw new Exception("Podano zły argument dla parametru \"-rate\"");
                        }
                    }
                    else
                        throw new Exception("Podano zły argument dla parametru \"-rate\"");
                    break;
                }
                case "-import":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1]))
                        importFilePath = Paths.get("").toAbsolutePath() + "/" + args[i+1];
                    else
                        throw new Exception("Podano zły argument dla parametru \"-import\"");
                    break;
                }
                case "-prediction":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1]))
                        predictionFilePath = Paths.get("").toAbsolutePath() + "/" + args[i+1];
                    else
                        throw new Exception("Podano zły argument dla parametru \"-prediction\"");
                    break;
                }

                default:{
                    throw new Exception("Podano zły parametr!!");
                }
            }
        }
    }

    public static Double getLearnRate() {
        return learnRate != null ? learnRate : 1.;
    }

    public static Integer getIterations() {
        return iterations != null ? iterations : 300;
    }

    public static String getTrainFilePath() { return trainFilePath; }

    public static String getTestFilePath() {
        return testFilePath;
    }

    public static String getImportFilePath() { return importFilePath; }

    public static String getPredictionFilePath() { return predictionFilePath; }
}
