package pl.kaliszewski.neuralNetworks;

import java.util.List;

public class ConfusionMatrix {
    private int matrix[][];
    private List<Integer> classes;
    private int countOfClasses;

    public ConfusionMatrix(List<Integer> classes) {
        matrix = new int[classes.size()][classes.size()];
        this.classes = classes;
        countOfClasses = this.classes.size();
    }

    public void addResult(int prediction, int fact){
        var predictionIndex = getIndex(prediction);
        var factIndex = getIndex(fact);
        matrix[predictionIndex][factIndex] += 1;
    }

    private int getIndex(int value){
        return classes.indexOf(value);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\t\t\tFact\n");

        builder.append("\t\t\t\t");
        for (int clas:classes) {
            builder.append(clas + "\t");
        }
        builder.append("\n");

        builder.append("Prediction\t"+classes.get(0)+"\t");
        for (int i = 0; i < countOfClasses ; i++) {
            if(i!=0)
                builder.append("\t\t\t" + classes.get(i) +"\t");

            for (int j = 0; j < countOfClasses; j++) {
                builder.append(matrix[i][j] + "\t");
            }
            builder.append("\n");
        }

        return builder.toString();
    }

    public void print(){
        System.out.println(this);
    }
}
