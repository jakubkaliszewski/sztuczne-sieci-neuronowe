package pl.kaliszewski.neuralNetworks.multiLayerPerceptron;

import pl.kaliszewski.neuralNetworks.ActivationFunctionEnum;
import pl.kaliszewski.neuralNetworks.Dataset;
import pl.kaliszewski.neuralNetworks.FeaturesSet;
import pl.kaliszewski.neuralNetworks.multiLayerPerceptron.layer.Layer;
import pl.kaliszewski.neuralNetworks.multiLayerPerceptron.layer.OutputLayer;

import java.util.ArrayList;

/**
 * Główna klasa obsługująca funkcjonalności sieci MLP.
 */
public class MultiLayerPerceptron {
    /**
     * Ilość warstw ukrytych w sieci MLP.
     */
    private int hiddenLayers;
    /**
     * Całkowita ilość warstw w sieci MLP.
     */
    private int layers;
    /**
     * Ilość perceptronów w każdej warstwie ukrytej.
     */
    private int perceptronsInHiddenLayers;
    /**
     * Zbiór danych wykorzystywany w celu treningu sieci MLP.
     */
    private Dataset dataset;
    /**
     * Lista warstw w sieci MLP.
     */
    private ArrayList<Layer> layersList;
    /**
     * Wybrana funkcja aktywacji wykorzystywana przez wszystkie perceptrony w sieci.
     */
    private ActivationFunctionEnum activationFunction;

    /**
     * Przechowuje informację, czy sieć MLP przeszła trening.
     */
    private boolean trained = false;

    /**
     * @param dataset Zbiór danych szkoleniowy.
     * @param layers Ilość warstw, prócz ostatniej (wynikowej).
     * @param perceptronsCountInHiddenLayer Ilość perceptronów w każdej z ukrytych warstw.
     * @param activationFunction Funkcja aktywacji używana przez wszystkie perceptrony w sieci MLP.
     * @throws Exception Wyjątek występuje, gdy layers oraz perceptronsCountInHiddenLayer są zerowe.
     */
    public MultiLayerPerceptron(Dataset dataset, int layers, int perceptronsCountInHiddenLayer, ActivationFunctionEnum activationFunction) throws Exception {

        if(layers == 0 || perceptronsCountInHiddenLayer == 0)
            throw new Exception("Parametry określające ilość warstw ukrytych oraz liczby perceptronów na warstwę jest wymagana!!");

        this.hiddenLayers = layers - 1;
        this.layers = layers + 1;//+1 na warstwę wyjściową
        this.perceptronsInHiddenLayers = perceptronsCountInHiddenLayer;
        this.dataset = dataset;
        this.activationFunction = activationFunction;

        initializeLayers();
    }

    /**
     * @return Zwraca ilość warstw ukrytych w sieci MLP.
     */
    public int getHiddenLayers() {
        return hiddenLayers;
    }

    /**
     * @return Zwraca ilość perceptronów stosowanych w każdej warstwie ukrytej sieci MLP.
     */
    public int getPerceptronsCountInHiddenLayers() {
        return perceptronsInHiddenLayers;
    }

    public boolean isTrained() {
        return trained;
    }

    /**
     * Inicjalizuje warstwy sieci MLP, odpowiednio według podanej ilości warstw oraz ilości perceptronów na warstwę.
     */
    private void initializeLayers(){
        this.layersList = new ArrayList<>(this.layers);
        layersList.add(new Layer(0, this.perceptronsInHiddenLayers, this.activationFunction, this.dataset.getCountOfFeatures()));//pierwsza warstwa ma tyle wag co ilość podanych cech

        /*Warstwy ukryte*/
        for (int index = 1; index <= hiddenLayers; index++) {
            layersList.add(new Layer(index, this.perceptronsInHiddenLayers, this.activationFunction, this.perceptronsInHiddenLayers));
        }

        layersList.add(new OutputLayer(this.layers -1, this.activationFunction, this.perceptronsInHiddenLayers));//ostatnia warstwa
    }

    /**
     * Rozpoczyna sekwencję treningu sieci MLP z uwzględnieniem podanych parametrów.
     * @param iterations Ilość iteracji w treningu.
     * @param learningRate Stała uczenia.
     */
    public void train(int iterations, double learningRate){
        FeaturesSet featuresSet;
        if(activationFunction == ActivationFunctionEnum.Sigmoid)
            dataset.normalize();

        for (int i = 0; i < iterations; i++) {
            featuresSet = dataset.getRandomVectorFeatures();
            for (var layer : layersList){
                layer.setInputFeaturesSet(featuresSet);
                featuresSet = layer.getOutputFeaturesSet();
            }

            //wsteczna propagacja błędu
            OutputLayer outputLayer = (OutputLayer)layersList.get(layers-1);
            outputLayer.updateWeightsByBackPropagation(null, learningRate);
            var iterator = layersList.listIterator(layers-1);
            var nextLayer = layersList.get(layers-1);
            do {
                var actualLayer = iterator.previous();
                actualLayer.updateWeightsByBackPropagation(nextLayer, learningRate);
                nextLayer = actualLayer;
            }while (iterator.hasPrevious());

        }

        trained = true;
    }

    /**
     * Zwraca informację zwrotną o wyniku predykcji.
     * @param featuresSet Wektor cech poddawanych predykcji.
     * @return Wynik predykcji będący klasą.
     */
    public double predictionResult(FeaturesSet featuresSet){

        for (var layer : layersList){
            layer.setInputFeaturesSet(featuresSet);
            featuresSet = layer.getOutputFeaturesSet();
        }

        OutputLayer output = (OutputLayer) layersList.get(layers-1);
        return output.getResult();
    }

    /**
     * Zwraca informację zwrotną o wyniku predykcji w czytelny dla użytkownika sposób.
     * @param featuresSet Wektor cech poddawanych predykcji.
     */
    public void predictionHumanReadable(FeaturesSet featuresSet){

    }


    /**
     * @param testDataset Testowy zbiór danych.
     * @throws Exception Wyjątek występuje, gdy nie przeprowadzono treningu dla sieci MLP.
     */
    public void test(Dataset testDataset) throws Exception {
        if(activationFunction == ActivationFunctionEnum.Sigmoid)
            testDataset.normalize();

        if(!isTrained())
            throw new Exception("Nie przeprowadzono treningu dla perceptronu!");
        int countOfPassed = 0;

        for (var features : testDataset.getEntireDataset()){
            double predictionResult = predictionResult(features);

            printResult(features, predictionResult);
            if (predictionResult == features.getClassName())
                countOfPassed++;
        }

        System.out.println(String.format("Skuteczność sieci MLP dla %s wynosi: %.2f %%", testDataset.getDatasetName(), (double)countOfPassed/(double)testDataset.getCountOfVectors() * 100.));
        /*
        Można dodać zliczanie rezultatów dla każdej z klas, macierz rezultatów.
        Dobra klasyfikacja, zła klasyfikacja-> jaką złą klasę wskazał. Da to pojęcie co z czym często było mylone.
    */
    }

    private void printResult(FeaturesSet featuresSet , double prediction){
        StringBuilder builder = new StringBuilder();

        builder.append("[");
        for(var feature : featuresSet.getFeatures()){
            builder.append(feature);
            builder.append(",");
        }
        builder.append("]: ");
        builder.append("Prediction: ");
        builder.append(String.format("%f", prediction));
        builder.append(" Class: ");
        builder.append(featuresSet.getClassName());

        System.out.println(builder.toString());
    }
}
