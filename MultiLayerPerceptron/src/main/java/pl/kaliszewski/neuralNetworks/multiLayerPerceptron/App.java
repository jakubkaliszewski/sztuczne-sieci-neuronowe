package pl.kaliszewski.neuralNetworks.multiLayerPerceptron;

import pl.kaliszewski.neuralNetworks.ConsoleArgumentsInterpreter;
import pl.kaliszewski.neuralNetworks.Dataset;

public class App {
    public static void main(String[] args) {
        Dataset trainDataset, testDataset;

        try {
            ConsoleArgumentsInterpreter.setParameters(args);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }

        trainDataset = new Dataset(ConsoleArgumentsInterpreter.getTrainFilePath());
        testDataset = new Dataset(ConsoleArgumentsInterpreter.getTestFilePath());

        try {
            MultiLayerPerceptron MLP = new MultiLayerPerceptron(
                    trainDataset,
                    ConsoleArgumentsInterpreter.getLayers(),
                    ConsoleArgumentsInterpreter.getPerceptrons(),
                    ConsoleArgumentsInterpreter.getActivationFunction()
            );

            MLP.train(ConsoleArgumentsInterpreter.getIterations(), ConsoleArgumentsInterpreter.getLearnRate());

            MLP.test(testDataset);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
