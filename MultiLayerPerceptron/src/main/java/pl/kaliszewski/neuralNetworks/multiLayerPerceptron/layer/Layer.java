package pl.kaliszewski.neuralNetworks.multiLayerPerceptron.layer;

import pl.kaliszewski.neuralNetworks.ActivationFunctionEnum;
import pl.kaliszewski.neuralNetworks.FeaturesSet;
import pl.kaliszewski.neuralNetworks.Perceptron;
import java.util.ArrayList;

/**
 * Klasa reprezentująca pojedynczą warstwę w sieci neuronowej.
 */
public class Layer {
    /**
     * Liczba perceptronów w warstwie.
     */
    protected final int perceptronsCount;
    /**
     * Numer porządkowy - identyfikacyjny warstwy.
     */
    protected final int numberOfLayer;
    /**
     * Cechy otrzymane na wejściu warstwy - cechy wyjściowe z poprzedniej warstwy.
     */
    protected FeaturesSet inputFeaturesSet;
    /**
     * Cechy wyznaczone w tej warstwie - będą cechami wejściowymi w kolejnej warstwie.
     */
    protected FeaturesSet outputFeaturesSet;
    /**
     * Lista Perceptronów w warstwie.
     * @see Perceptron
     */
    protected ArrayList<Perceptron> perceptrons;
    /**
     * Flaga informująca czy warstwa wyznaczyła swój wyjściowy wektor cech.
     */
    protected boolean predicated;
    /**
     * Funkcja aktywacji używanana w warstwie.
     * @see ActivationFunctionEnum
     */
    protected ActivationFunctionEnum activationFunctionEnum;
    protected double[] localError;
    protected double[] perceptronsSums;
    protected double bias;

    /**
     * @param numberOfLayer Numer warswy w sieci neuronowej.
     * @param perceptronsCount Liczba perceptronów w warstwie.
     */
    public Layer(int numberOfLayer, int perceptronsCount, ActivationFunctionEnum activationFunctionEnum, int perceptronsInPreviousLayer) {
        this.numberOfLayer = numberOfLayer;
        this.perceptronsCount = perceptronsCount;
        this.predicated = false;
        this.activationFunctionEnum = activationFunctionEnum;
        this.localError = new double[this.perceptronsCount];
        this.perceptronsSums = new double[this.perceptronsCount];
        this.bias = Perceptron.random();

        initializePerceptrons(perceptronsInPreviousLayer);
    }

    /**
     * Inicjalizuje perceptrony w warstwie. Założenie, że warstwy ukryte mają taką samą ilość neuronów.
     */
    private void initializePerceptrons(int perceptronsInPreviousLayer){
        this.perceptrons = new ArrayList<>(this.perceptronsCount);

        for (int i = 0; i < perceptronsCount; i++) {
            this.perceptrons.add(new Perceptron(perceptronsInPreviousLayer));
        }
    }

    /**
     * @return Zwraca numer warstwy.
     */
    public int getNumberOfLayer() {
        return numberOfLayer;
    }

    /**
     * @param inputFeaturesSet Zestaw cech wejściowych na którym będzie przebiegać iteracja treningu (uczenia).
     */
    public void setInputFeaturesSet(FeaturesSet inputFeaturesSet) {
        this.inputFeaturesSet = inputFeaturesSet;
        this.predicated = false;
    }

    /**
     * @return Zestaw cech wyjściowy dla warstwy. Otrzymano go z wyników perceptronów w warstwie.
     */
    public FeaturesSet getOutputFeaturesSet() {
        if(predicated)
            return outputFeaturesSet;
        else {
            predicate();
            return getOutputFeaturesSet();
        }
    }

    public ArrayList<Perceptron> getPerceptrons() {
        return perceptrons;
    }

    /**
     * Zwraca nowy zestaw cech będący wynikami z każdego perceptronu w warstwie.
     * Zestaw ten służy jako wejściowy zestaw cech dla kolejnej warstwy sieci neuronowej.
     */
    protected void predicate() {
        double [] inputFeatures = inputFeaturesSet.getFeatures();
        double [] outputFeatures = new double[this.perceptronsCount];
        double sum;

        for (int i = 0; i < perceptronsCount; i++) {
            var weights = perceptrons.get(i).getWeights();
            //var bias = perceptrons.get(i).getBias();
            sum = 0.;

            for (int j = 0; j < inputFeatures.length; j++) {
                sum += (weights[j] * inputFeatures[j]);// + bias;
            }

            perceptronsSums[i] = sum;
            outputFeatures[i] = activationFunctionEnum.getFunction().apply(sum);
        }

        this.outputFeaturesSet = new FeaturesSet(outputFeatures, inputFeaturesSet.getClassName());
        this.predicated = true;
    }

    /**
     * @return Zwraca wektor błędów dla każdego z neuronów w warstwie.
     * */
    public double[] getErrors() {
        return localError;
    }

    /**
     * @return Zwraca ilość perceptronów w warstwie sieci neuronowej.
     */
    public int getPerceptronsCount() {
        return perceptronsCount;
    }

    /**
     * Uaktualnia w warstwie wagi perceptronów na podstawie algorytmu wstecznej propagacji.
     * Założenie, że warstwy ukryte mają identyczną ilość perceptronów.
     * @param nextLayer Warstwa "następna", wymagana do obliczenia błędu aktualnej warstwy z powodu posiadania wag połączeń między tymi wagami.
     * @param learningRate Stała uczenia.
     */
    public void updateWeightsByBackPropagation(Layer nextLayer, double learningRate) {//na podstawie wstecznej propagacji błędów
        determinateLocalError(nextLayer);

        for (int i = 0; i < perceptronsCount; i++) {
            var actualPerceptron = this.perceptrons.get(i);
            var weights = actualPerceptron.getWeights();
            var input = inputFeaturesSet.getFeatures();

            for (int j = 0; j < weights.length; j++) {
                weights[j] -= learningRate * this.localError[i] * input[j];
            }

            actualPerceptron.setWeights(weights);
        }

    }

    private void determinateLocalError(Layer nextLayer){
        var errorFromNextLayer = nextLayer.getErrors();
        var perceptronsFromNextLayer = nextLayer.getPerceptrons();
        for (int i = 0; i < perceptronsCount; i++){
            double sum = 0.;

            for (int j = 0; j < errorFromNextLayer.length; j++) {
                var actualPerceptronWeights = perceptronsFromNextLayer.get(j).getWeights();
                sum += errorFromNextLayer[j] * actualPerceptronWeights[i] * activationFunctionEnum.getDerivative().apply(perceptronsSums[i]);
            }

            this.localError[i] = sum;
        }
    }
}
