package pl.kaliszewski.neuralNetworks.multiLayerPerceptron.layer;

import pl.kaliszewski.neuralNetworks.ActivationFunctionEnum;
import pl.kaliszewski.neuralNetworks.FeaturesSet;

public class OutputLayer extends Layer {

    private double result;
    /**
     * @param numberOfLayer    Numer warswy w sieci neuronowej.
     * @param activationFunctionEnum Wybrana funkcja aktywacji dla neuronów w warstwie.
     */
    public OutputLayer(int numberOfLayer, ActivationFunctionEnum activationFunctionEnum, int perceptronsInPreviousLayer) {//wag tyle samo co wartości featuresSet
        super(numberOfLayer,  1, activationFunctionEnum, perceptronsInPreviousLayer);
    }

    public double getResult() {
        if (predicated){
            var features = this.outputFeaturesSet.getFeatures();
            this.result = features[0];
            return this.result;
        }
        else {
            predicate();
            return getResult();
        }
    }

    @Deprecated
    public double getLocalError(){
        if(predicated){
            determinateLocalError();
            return localError[0];
        }
        else {
            predicate();
            return getLocalError();
        }

    }

    public double[] getLocalErrors(){
        if(predicated){
            determinateLocalError();
            return localError;
        }
        else {
            predicate();
            return getLocalErrors();
        }
    }

    @Override
    protected void predicate(){
        double [] inputFeatures = inputFeaturesSet.getFeatures();
        double [] outputFeatures = new double[this.perceptronsCount];
        double sum;

        for (int i = 0; i < perceptronsCount; i++) {
            var weights = perceptrons.get(i).getWeights();
            //var bias = perceptrons.get(i).getBias();
            sum = 0.;

            for (int j = 0; j < inputFeatures.length; j++) {
                sum += (weights[j] * inputFeatures[j]);// + bias;
            }

            perceptronsSums[i] = sum;
            var output = activationFunctionEnum.getFunction().apply(sum);
            if(activationFunctionEnum == ActivationFunctionEnum.Sigmoid)
                outputFeatures[i] = output < 0.5 ? -1. : 1.;
            else
                outputFeatures[i] = output;
        }

        this.outputFeaturesSet = new FeaturesSet(outputFeatures, inputFeaturesSet.getClassName());
        this.predicated = true;
    }

    private void determinateLocalError(){
        for (int i = 0; i < perceptronsCount; i++) {
            localError[i] = activationFunctionEnum.getDerivative().apply(perceptronsSums[i]) * (inputFeaturesSet.getClassName() - getResult());
        }
    }

    /**
     * Uaktualnia w warstwie wagi perceptronów na podstawie algorytmu wstecznej propagacji.
     * Założenie, że warstwy ukryte mają identyczną ilość perceptronów.
     * @param nextLayer Warstwa "następna", tu null
     * @param learningRate Stała uczenia.
     */
    @Override
    public void updateWeightsByBackPropagation(Layer nextLayer, double learningRate) {//na podstawie wstecznej propagacji błędów
        determinateLocalError();

        for (int i = 0; i < perceptronsCount; i++) {
            var actualPerceptron = this.perceptrons.get(i);
            var weights = actualPerceptron.getWeights();
            var input = inputFeaturesSet.getFeatures();

            for (int j = 0; j < weights.length; j++) {
                weights[j] -= learningRate * this.localError[i] * input[j];
            }

            actualPerceptron.setWeights(weights);
        }

    }
}