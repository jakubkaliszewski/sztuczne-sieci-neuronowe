package pl.kaliszewski.neuralNetworks;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Zawiera zbiór danych uprzednio wczytany z pliku
 */
public class Dataset {

    private int countOfFeatures;
    private int countOfVectors;
    private int countOfClasses;
    private String datasetName;

    private ArrayList<FeaturesSet> ListOfFeatures;

    public Dataset(String filename) {
        try {
            loadDatasetFromFile(filename);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Wczytuje spreparowany plik z danymi. Na jego podstawie ustawiane są takie parametry jak:
     * <ul>
     *     <li>liczba cech,</li>
     *     <li>liczba wektorów,</li>
     *     <li>liczba klas do klasyfikacji</li>
     * </ul>
     * @param filename Plik z danymi do wczytania
     */
    private void loadDatasetFromFile(String filename) throws Exception {
        if(filename == null)
            throw new Exception("Nie podano plików źródłowych!");

        List<Integer> classes = new LinkedList<>();
        datasetName = filename;

        try (Stream<String> lines = Files.lines(Paths.get(filename), Charset.defaultCharset())) {
            ListOfFeatures = new ArrayList<>(countOfVectors);

            lines.forEach(line -> {
                if(!line.isEmpty()){
                    String[] splitedLine = line.split("[ ,\t]");
                    countOfFeatures = splitedLine.length - 1;
                    double[] vector = new double[countOfFeatures];

                    for (int i = 0; i < countOfFeatures; i++) {
                        if(!splitedLine[i].isEmpty())
                            vector[i] = Double.parseDouble(splitedLine[i]);
                    }

                    int sampleClass = Integer.parseInt(splitedLine[splitedLine.length - 1]);
                    if(!classes.contains(sampleClass))
                        classes.add(sampleClass);

                    FeaturesSet set = new FeaturesSet(vector, sampleClass);
                    ListOfFeatures.add(set);
                    countOfVectors++;
                }
            });

            countOfClasses = classes.size();
        }
        catch (IOException exception){
            System.out.printf("Błąd odczytu pliku %s! Wyjątek: %s", filename, exception.toString());
            throw exception;
        }
    }

    public FeaturesSet getRandomVectorFeatures() {
        int countOfElements = ListOfFeatures.size();
        return ListOfFeatures.get(random(countOfElements));
    }

    public ArrayList<FeaturesSet> getEntireDataset(){
        return this.ListOfFeatures;
    }

    private int random(int countOfElements){
        double random = Math.random() * (double) countOfElements;
        return (int)random;
    }

    public void normalize(){
        //normalizować kolumnami
        for (int i = 0; i < countOfFeatures; i++) {//zmiana kolumn
            double max = Double.MIN_VALUE;
            double min = Double.MAX_VALUE;

            for (int j = 0; j < countOfVectors; j++) {//zmiana wierszy - szukanie min i max
                var element = ListOfFeatures.get(j).getFeatures()[i];

                if(element >= max)
                    max = element;

                if(element < min)
                    min = element;
            }

            for (int j = 0; j < countOfVectors; j++) {//faktyczna normalizacja elementów w kolumnie
                var featuresSet = ListOfFeatures.get(j);
                var features = featuresSet.getFeatures();

                features[i] = (features[i] - min) / (max - min);
                //featuresSet.setFeatures(features);
                //ListOfFeatures.set(j, );//.get(j).getFeatures()[i];
            }

        }
    }



    public int getCountOfClasses() {
        return countOfClasses;
    }

    public int getCountOfFeatures() {
        return countOfFeatures;
    }

    public String getDatasetName() {
        return datasetName;
    }

    public int getCountOfVectors() {
        return countOfVectors;
    }
}
