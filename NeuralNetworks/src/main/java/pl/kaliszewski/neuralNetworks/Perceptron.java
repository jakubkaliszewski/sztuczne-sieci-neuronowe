package pl.kaliszewski.neuralNetworks;

public class Perceptron {
    protected double [] weights;
    protected double bias;
    protected int perceptronsInPreviousLayer;

    public Perceptron(int perceptronsInPreviousLayer) {
        this.perceptronsInPreviousLayer = perceptronsInPreviousLayer;
        initializeWeights();
    }

    /**
     * Inicjalizuje wektor wag oraz bias dla pojedynczego Perceptronu
     */
    protected void initializeWeights(){
        weights = new double[this.perceptronsInPreviousLayer];

        for (int i = 0; i < perceptronsInPreviousLayer; i++) {
            weights[i] = random();
        }
        bias = random();
    }

    public static double random(){
        double range = (0.1 - (-0.1));
        return Math.random() * range -0.1;
    }

    public double[] getWeights() {
        return weights;
    }

    public void setWeights(double[] weights) {
        this.weights = weights;
    }

    public double getBias() {
        return bias;
    }

    public void setBias(double bias) {
        this.bias = bias;
    }
}
