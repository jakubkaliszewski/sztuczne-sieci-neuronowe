package pl.kaliszewski.neuralNetworks;

import java.util.function.Function;

public enum ActivationFunctionEnum {
    Sigmoid("Sigmoid", ActivationFunctionEnum::Sigmoid, ActivationFunctionEnum::SigmoidDerivative),
    RELu("RELu", ActivationFunctionEnum::RELu, ActivationFunctionEnum::RELuDerivative),
    TanH("TanH" , ActivationFunctionEnum::TanH, ActivationFunctionEnum::TanHDerivative);

    private Function<Double, Double> function;
    private Function<Double, Double> derivative;
    private String name;

    ActivationFunctionEnum(String name, Function<Double, Double> function, Function<Double, Double> derivative) {
        this.function = function;
        this.name = name;
        this.derivative = derivative;
    }

    public Function<Double, Double> getFunction() {
        return function;
    }

    public Function<Double, Double> getDerivative() {
        return derivative;
    }

    public static double Sigmoid(double x){
        if (x >= 0)
            return 1;
        else
            return -1;
    }

    public static double SigmoidDerivative(double x){
        var result = Sigmoid(x);
        return result * (1. - result);
    }

    public static double RELu(double x){
        return x > 0. ? x : 0.;
    }

    public static double RELuDerivative(double x){
        return x > 0. ? 1. : 0.;
    }

    public static double TanH(double x){
        return Math.tanh(x);
    }

    public static double TanHDerivative(double x){
        return 1. - Math.pow(TanH(x), 2);
    }

    public String getName() {
        return name;
    }
}
