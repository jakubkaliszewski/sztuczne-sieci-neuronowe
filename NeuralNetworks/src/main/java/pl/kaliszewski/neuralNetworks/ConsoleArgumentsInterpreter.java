package pl.kaliszewski.neuralNetworks;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class ConsoleArgumentsInterpreter {

    private static Double learnRate;
    private static Integer iterations;
    private static Integer layers;
    private static Integer perceptrons;
    private static String trainFilePath;
    private static String testFilePath;
    private static ActivationFunctionEnum activationFunction;
    private final static List<String> parameters = Arrays.asList("-train", "-test", "-iter", "-rate", "-layers", "-perceptrons", "-activation");

    public static void setParameters(String[] args) throws Exception {
        for (int i = 0; i < args.length; i+=2) {
            switch (args[i]){
                case "-train":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1]))
                        trainFilePath = Paths.get("").toAbsolutePath() + "/" + args[i+1];
                    else
                        throw new Exception("Podano zły argument dla parametru \"-train\"");
                    break;
                }
                case "-test":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1]))
                        testFilePath = Paths.get("").toAbsolutePath() + "/" + args[i+1];
                    else
                        throw new Exception("Podano zły argument dla parametru \"-test\"");
                    break;
                }
                case "-iter":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1])) {
                        try {
                            iterations = Integer.parseInt(args[i+1]);
                        }
                        catch (Exception e){
                            throw new Exception("Podano zły argument dla parametru \"-iter\"");
                        }
                    }
                    else
                        throw new Exception("Podano zły argument dla parametru \"-iter\"");
                    break;
                }
                case "-rate":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1])){
                        try {
                            learnRate = Double.parseDouble(args[i+1]);
                        }
                        catch (Exception e){
                            throw new Exception("Podano zły argument dla parametru \"-rate\"");
                        }
                    }
                    else
                        throw new Exception("Podano zły argument dla parametru \"-rate\"");
                    break;
                }
                case "-layers":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1])){
                        try {
                            layers = Integer.parseInt(args[i+1]);
                        }
                        catch (Exception e){
                            throw new Exception("Podano zły argument dla parametru \"-layers\"");
                        }
                    }
                    else
                        throw new Exception("Podano zły argument dla parametru \"-layers\"");
                    break;
                }
                case "-perceptrons":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1])){
                        try {
                            perceptrons = Integer.parseInt(args[i+1]);
                        }
                        catch (Exception e){
                            throw new Exception("Podano zły argument dla parametru \"-perceptrons\"");
                        }
                    }
                    else
                        throw new Exception("Podano zły argument dla parametru \"-perceptrons\"");
                    break;
                }
                case "-activation":{
                    if (!args[i+1].isBlank() && !parameters.contains(args[i+1])){
                        try {
                            int index = i+1;
                            activationFunction = Arrays.stream(ActivationFunctionEnum.values()).filter(element -> element.getName().equalsIgnoreCase(args[index])).findFirst().get();
                        }
                        catch (Exception e){
                            throw new Exception("Podano zły argument dla parametru \"-activation\"");
                        }
                    }
                    else
                        throw new Exception("Podano zły argument dla parametru \"-activation\"");
                    break;
                }

                default:{
                    throw new Exception("Podano zły parametr!!");
                }
            }
        }
    }

    public static Double getLearnRate() {
        return learnRate != null ? learnRate : 1.;
    }

    public static Integer getIterations() {
        return iterations != null ? iterations : 300;
    }

    public static String getTrainFilePath() {
        return trainFilePath;
    }

    public static String getTestFilePath() {
        return testFilePath;
    }

    public static Integer getLayers() {
        return layers != null ? layers : 0;
    }

    public static Integer getPerceptrons() {
        return perceptrons != null ? perceptrons : 0;
    }

    public static ActivationFunctionEnum getActivationFunction() {
        return activationFunction != null ? activationFunction : ActivationFunctionEnum.Sigmoid;
    }
}
