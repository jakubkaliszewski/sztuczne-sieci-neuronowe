package pl.kaliszewski.neuralNetworks;

public class FeaturesSet {
    private double[] features;
    private int countOfFeatures;
    private int className;

    public FeaturesSet(double[] features, int className) {
        this.features = features;
        this.className = className;
        countOfFeatures = this.features.length;
    }

    public void setFeatures(double[] features) {
        this.features = features;
    }

    public double[] getFeatures() {
        return features;
    }

    public int getClassName() {
        return className;
    }

    public int getCountOfFeatures() { return countOfFeatures; }
}
