# encoding: utf-8

from keras.datasets import mnist
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import LabelEncoder, MinMaxScaler
from sklearn.metrics import confusion_matrix
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Dense

(x_train, y_train), (x_test, y_test) = mnist.load_data()

seed = 23
np.random.seed(seed)

pixel_count = x_train.shape[1] * x_train.shape[2]
x_train = x_train.reshape(x_train.shape[0], pixel_count).astype("float32")
x_test = x_test.reshape(x_test.shape[0], pixel_count).astype("float32")

x_train = MinMaxScaler().fit_transform(x_train)
x_test = MinMaxScaler().fit_transform(x_test)

y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

class_count = y_train.shape[1]

model = Sequential()
model.add(Dense(pixel_count, input_dim=pixel_count,
                kernel_initializer='normal', activation='relu'))
model.add(Dense(class_count, kernel_initializer='normal', activation='softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer='adam', metrics=['accuracy'])
model.fit(x_train, y_train, epochs=10, batch_size=200, verbose=2)
#scores = model.evaluate(x_test, y_test)
#print(scores[1] * 100)

y_labels = model.predict(x_test, batch_size=100)
conf_matrix = confusion_matrix(
    np.argmax(y_test, axis=1), np.argmax(y_labels, axis=1))##przeanalizować macierz

print(conf_matrix)

#for i in range(64):
#   plt.subplot(8, 8, i + 1)
#  plt.imshow(x_train[i], cmap = plt.get_cmap('gray'))
#plt.show()
