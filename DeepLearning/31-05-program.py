# encoding: utf-8
import pandas
import numpy as np
import keras
from sklearn.preprocessing import LabelEncoder, StandardScaler
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import StratifiedKFold, cross_val_score, KFold
from keras.optimizers import SGD

def create_model():
    model = Sequential()
    hiddenLayerCount = 2
    outputLayerCount = 3
    #model.add(Dense(hiddenLayerCount, activation = 'relu', input_dim = 4))
    #model.add(Dense(outputLayerCount, activation ='softmax'))
    model.add(Dense(outputLayerCount, activation ='softmax', input_dim = 4))    #tylko jedna warstwa
    #model.compile(loss = 'categorical_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
    sgd = SGD(lr = 0.1, decay = 1e-6, momentum = 0.9, nesterov = False)
    model.compile(loss = 'categorical_crossentropy', optimizer = sgd, metrics = ['accuracy'])
    #model.compile(loss = 'mean_squared_error', optimizer = sgd, metrics = ['accuracy'])    #label 
    return model

seed = 5
np.random.seed(seed)

ds = pandas.read_csv("https://fizyka.umk.pl/~kdobosz/nn/data/iris.data", header = None)
ds = ds.values

x = ds[:, 0 : -1].astype(float)
x = StandardScaler().fit_transform(x)
y = ds[:, -1]
y = LabelEncoder().fit_transform(y)
y = keras.utils.to_categorical(y)

classifier = KerasClassifier(build_fn = create_model, epochs = 200, batch_size = 5, verbose = 0)
kfold = KFold(n_splits = 10, shuffle = True, random_state = seed)
result = cross_val_score(classifier, x, y, cv = kfold)

print("Results: %.2f%% (%.2f%%)" % (result.mean() * 100, result.std() * 100))
