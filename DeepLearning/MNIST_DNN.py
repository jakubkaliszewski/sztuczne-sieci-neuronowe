# encoding: utf-8
#https://machinelearningmastery.com/save-load-keras-deep-learning-models/ zapis/import modelu swojego

from confusion_matrix_pretty_print import pretty_plot_confusion_matrix
from pandas import DataFrame

from keras.datasets import mnist
import keras
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import LabelEncoder, MinMaxScaler
from sklearn.metrics import confusion_matrix
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.utils import to_categorical


(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train = x_train.reshape(x_train.shape[0], 1, 28, 28).astype('float32')
x_test = x_test.reshape(x_test.shape[0], 1, 28, 28).astype('float32')
x_train /= 256
x_test /= 256
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

class_count = y_train.shape[1]

model = Sequential()
model.add(Conv2D(32, (5, 5), input_shape=(1, 28, 28),
                 data_format='channels_first', activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.2))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dense(class_count, activation='softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer='adam', metrics=['accuracy'])
model.fit(x_train, y_train, epochs=10, batch_size=200, verbose=2)
#scores = model.evaluate(x_test, y_test)
#print(scores[1] * 100)

y_labels = model.predict(x_test, batch_size=100)
conf_matrix = confusion_matrix(
    np.argmax(y_test, axis=1), np.argmax(y_labels, axis=1))  # przeanalizować macierz

df_cm = DataFrame(conf_matrix, index=range(class_count), columns=range(class_count))
pretty_plot_confusion_matrix(df_cm, cmap='BuPu', figsize=(10, 10), fz=12)
